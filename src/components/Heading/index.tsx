import React, { ReactNode } from "react";
import { TextStyle, Text as TextBase } from "react-native";
import { Fonts } from "../../theme";
import { extractTextStyleProps } from "../../utils/styles";

interface Props extends TextStyle {
  children?: ReactNode;
}

export const Heading = ({ children, ...props }: Props): JSX.Element => {
  const style = extractTextStyleProps(props);

  return (
    <TextBase
      style={{
        fontFamily: Fonts.Black,
        ...style,
      }}
    >
      {children}
    </TextBase>
  );
};
