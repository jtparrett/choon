import * as Yup from "yup";

const validateError = Yup.object({
  message: Yup.string().required(),
});

export const errorWithMessage = (error: unknown) => {
  return validateError.validateSync(error);
};
