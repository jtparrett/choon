import { useNavigation } from "@react-navigation/native";
import { StatusBar } from "expo-status-bar";
import React from "react";
import { SafeAreaView } from "react-native";

import { Box, Button, Heading, Text } from "../../components";
import { Colors, Fonts, Sizes } from "../../theme";
import { SCREENS } from "../../utils/screens";

export const LandingScreen = (): JSX.Element => {
  const navigation = useNavigation();

  return (
    <Box height="100%" backgroundColor={Colors.white}>
      <StatusBar style="light" />
      <Box
        flex={1}
        padding={Sizes[9]}
        justifyContent="flex-end"
        backgroundColor={Colors.black}
      >
        <Heading fontSize={Sizes[8]} color={Colors.white}>
          CHOON
        </Heading>
        <Text
          color={Colors.gray[500]}
          fontFamily={Fonts.Bold}
          fontSize={Sizes[5]}
        >
          Musician Discovery
        </Text>
      </Box>
      <SafeAreaView>
        <Box padding={Sizes[9]}>
          <Button
            marginBottom={Sizes[3]}
            onPress={() => {
              // @ts-ignore
              navigation.navigate(SCREENS.SIGN_IN);
            }}
          >
            Sign in
          </Button>
          <Button
            variant="secondary"
            onPress={() => {
              // @ts-ignore
              navigation.navigate(SCREENS.SIGN_UP);
            }}
          >
            Create a new account
          </Button>
        </Box>
      </SafeAreaView>
    </Box>
  );
};
