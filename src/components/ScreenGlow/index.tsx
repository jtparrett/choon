import React from "react";
import Svg, { Defs, RadialGradient, Stop, Circle } from "react-native-svg";
import { Box } from "..";

export const ScreenGlow = (): JSX.Element => {
  return (
    <Box
      position="absolute"
      top={0}
      left={0}
      width="100%"
      height="100%"
      overflow="hidden"
    >
      <Svg width="100%" height="100%" viewBox="0 0 100 100">
        <Defs>
          <RadialGradient id="grad1">
            <Stop offset="0" stopColor="#33ADFF" stopOpacity="1" />
            <Stop offset="1" stopColor="#33ADFF" stopOpacity="0" />
          </RadialGradient>

          <RadialGradient id="grad2">
            <Stop offset="0" stopColor="#FF66FF" stopOpacity="1" />
            <Stop offset="1" stopColor="#FF66FF" stopOpacity="0" />
          </RadialGradient>
        </Defs>
        <Circle cx="35" cy="35" r="50" fill="url(#grad1)" opacity={0.7} />
        <Circle cx="65" cy="75" r="50" fill="url(#grad2)" opacity={0.7} />
      </Svg>
    </Box>
  );
};
