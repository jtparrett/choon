export const oneOf = <T extends string>(
  value: string,
  values: T[]
): T | undefined => values.find((a) => a === value);
