import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import { SCREENS } from "../../utils/screens";
import { LandingScreen } from "../Landing";
import { SigninScreen } from "../Signin";
import { SignupScreen } from "../Signup";

const Stack = createNativeStackNavigator();

export const AuthScreens = (): JSX.Element => {
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName={SCREENS.LANDING}
    >
      <Stack.Screen name={SCREENS.LANDING} component={LandingScreen} />
      <Stack.Screen name={SCREENS.SIGN_IN} component={SigninScreen} />
      <Stack.Screen name={SCREENS.SIGN_UP} component={SignupScreen} />
    </Stack.Navigator>
  );
};
