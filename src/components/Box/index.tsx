import React, { ReactNode } from "react";
import { View, ViewStyle } from "react-native";
import { extractViewStyleProps } from "../../utils/styles";

export interface BoxProps extends ViewStyle {
  children?: ReactNode;
}

export const Box = ({ children, ...props }: BoxProps): JSX.Element => {
  const style = extractViewStyleProps(props);

  return <View style={style}>{children}</View>;
};
