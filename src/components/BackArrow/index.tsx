import React from "react";
import { useNavigation } from "@react-navigation/core";
import { TouchableOpacity, ViewStyle } from "react-native";
import { ArrowLeftIcon } from "react-native-heroicons/solid";

import { Colors, Sizes } from "../../theme";

const hitSlop = Sizes[2];
const iconSize = Sizes[6];

interface Props extends ViewStyle {
  color?: string;
}

export const BackArrow = ({
  color = Colors.black,
  ...style
}: Props): JSX.Element => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={navigation.goBack}
      activeOpacity={0.8}
      hitSlop={{
        top: hitSlop,
        right: hitSlop,
        bottom: hitSlop,
        left: hitSlop,
      }}
      style={[
        {
          width: iconSize,
        },
        style,
      ]}
    >
      <ArrowLeftIcon color={color} size={iconSize} />
    </TouchableOpacity>
  );
};
