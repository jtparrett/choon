import React from "react";
import { Colors, Sizes } from "../../theme";
import { Heading } from "../Heading";

export const UserQueryError = (): JSX.Element => {
  return (
    <Heading color={Colors.error} fontSize={Sizes[4]}>
      Uh oh!{"\n"}Something went wrong.
    </Heading>
  );
};
