import { useNavigation } from "@react-navigation/core";
import React from "react";
import { SafeAreaView } from "react-native";
import { useMutation } from "react-query";
import { Box, Button, Logo } from "../../components";
import { Colors, Sizes } from "../../theme";
import { SCREENS } from "../../utils/screens";
import { supabase } from "../../utils/supabase";

export const AccountScreen = (): JSX.Element => {
  const navigation = useNavigation();
  const signoutMutation = useMutation(async () => {
    await supabase.auth.signOut();
  });

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <Box padding={Sizes[9]}>
        <Logo subText="Account" marginBottom={Sizes[6]} />

        <Button
          variant="secondary"
          marginBottom={Sizes[3]}
          onPress={() => navigation.navigate(SCREENS.EDIT_PROFILE)}
        >
          Edit your profile
        </Button>

        <Button
          variant="secondary"
          onPress={() => signoutMutation.mutate()}
          isLoading={signoutMutation.isLoading}
          disabled={signoutMutation.isLoading}
        >
          Sign out
        </Button>
      </Box>
    </SafeAreaView>
  );
};
