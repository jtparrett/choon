import {
  BottomTabBarProps,
  createBottomTabNavigator,
} from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import { SafeAreaView, TouchableOpacity } from "react-native";
import {
  ChatAlt2Icon,
  CheckCircleIcon,
  CogIcon,
  UserCircleIcon,
} from "react-native-heroicons/solid";

import { Box } from "../../components";
import { useSession } from "../../contexts/session";
import { Colors, Sizes } from "../../theme";
import { SCREENS } from "../../utils/screens";
import { AccountScreen } from "../Account";
import { DiscoverScreen } from "../Discover";
import { EditProfileScreen } from "../EditProfile";
import { InboxScreen } from "../Inbox";
import { LikesScreen } from "../Likes";
import { ProfileScreen } from "../Profile";

const iconsByIndex = [UserCircleIcon, CheckCircleIcon, ChatAlt2Icon, CogIcon];

const TabBar = ({ navigation, state }: BottomTabBarProps): JSX.Element => {
  return (
    <SafeAreaView style={{ backgroundColor: Colors.white }}>
      <Box
        flexDirection="row"
        justifyContent="space-evenly"
        paddingVertical={Sizes[4]}
      >
        {state.routes.map((route, index) => {
          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: "tabPress",
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate({ name: route.name, params: {} });
            }
          };

          const Icon = iconsByIndex[index];

          return (
            <TouchableOpacity
              activeOpacity={1}
              key={route.key}
              onPress={onPress}
              hitSlop={{
                top: Sizes[4],
                right: Sizes[4],
                bottom: Sizes[4],
                left: Sizes[4],
              }}
            >
              <Icon
                color={isFocused ? Colors.black : Colors.gray[500]}
                size={Sizes[8]}
              />
            </TouchableOpacity>
          );
        })}
      </Box>
    </SafeAreaView>
  );
};

const Tab = createBottomTabNavigator();

const RootTabs = (): JSX.Element => {
  return (
    <Tab.Navigator
      initialRouteName={SCREENS.DISCOVER}
      screenOptions={{
        headerShown: false,
      }}
      tabBar={TabBar}
    >
      <Tab.Screen name={SCREENS.DISCOVER} component={DiscoverScreen} />
      <Tab.Screen name={SCREENS.LIKES} component={LikesScreen} />
      <Tab.Screen name={SCREENS.INBOX} component={InboxScreen} />
      <Tab.Screen name={SCREENS.ACCOUNT} component={AccountScreen} />
    </Tab.Navigator>
  );
};

export type RootStackParamList = {
  Root: undefined;
  Profile: { userId: string };
  EditProfile: undefined;
};

const Stack = createNativeStackNavigator<RootStackParamList>();

export const RootScreens = (): JSX.Element => {
  const session = useSession();

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={SCREENS.ROOT}
    >
      <Stack.Screen name={SCREENS.ROOT} component={RootTabs} />
      <Stack.Screen
        name={SCREENS.PROFILE}
        component={ProfileScreen}
        initialParams={{ userId: session?.user?.id }}
      />
      <Stack.Screen name={SCREENS.EDIT_PROFILE} component={EditProfileScreen} />
    </Stack.Navigator>
  );
};
