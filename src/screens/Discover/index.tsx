import { useNavigation } from "@react-navigation/native";
import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  ActivityIndicator,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import { CheckIcon, XIcon } from "react-native-heroicons/solid";
import { useQuery } from "react-query";

import {
  Box,
  IconButton,
  Logo,
  ProfileCard,
  Heading,
  UserQueryError,
} from "../../components";
import { ScreenGlow } from "../../components/ScreenGlow";
import { Colors, Radius, Sizes } from "../../theme";
import { definitions } from "../../types/supabase";
import { useDislikeProfile } from "../../utils/hooks/useDislikeProfile";
import { useLikeProfile } from "../../utils/hooks/useLikeProfile";
import { SCREENS } from "../../utils/screens";
import { supabase } from "../../utils/supabase";

export const DiscoverScreen = (): JSX.Element => {
  const navigation = useNavigation();
  const discoverQuery = useQuery("discover", async () => {
    const { data, error } = await supabase
      .rpc<definitions["profiles"]>("user_undiscovered_profiles")
      .limit(1)
      .maybeSingle();

    if (error) {
      throw error;
    }

    return data;
  });

  const onSuccess = async () => {
    await discoverQuery.refetch();
  };

  const TopProfile = (): JSX.Element => {
    const likeProfileMutation = useLikeProfile({
      onSuccess,
    });
    const dislikeProfileMutation = useDislikeProfile({
      onSuccess,
    });

    if (discoverQuery.isLoading || discoverQuery.isIdle) {
      return <ActivityIndicator size="large" color={Colors.black} />;
    }

    if (discoverQuery.isError) {
      return <UserQueryError />;
    }

    if (discoverQuery.data === null) {
      return (
        <Heading fontSize={Sizes[4]}>
          You've run out of profiles to view,{"\n"}check back later.
        </Heading>
      );
    }

    const profile = discoverQuery.data;

    return (
      <>
        <TouchableOpacity
          activeOpacity={0.8}
          style={{
            overflow: "hidden",
            borderRadius: Radius.lg,
          }}
          onPress={() => {
            // @ts-ignore
            navigation.navigate(SCREENS.PROFILE, {
              userId: profile.id,
            });
          }}
        >
          <ProfileCard profile={profile} />
        </TouchableOpacity>
        <Box
          flexDirection="row"
          justifyContent="space-around"
          marginTop={Sizes[6]}
        >
          <IconButton
            icon={<XIcon size={Sizes[8]} color={Colors.white} />}
            isLoading={dislikeProfileMutation.isLoading}
            disabled={dislikeProfileMutation.isLoading}
            onPress={() => dislikeProfileMutation.mutate(profile.id)}
          />
          <IconButton
            icon={<CheckIcon size={Sizes[8]} color={Colors.white} />}
            isLoading={likeProfileMutation.isLoading}
            disabled={likeProfileMutation.isLoading}
            onPress={() => likeProfileMutation.mutate(profile.id)}
          />
        </Box>
      </>
    );
  };

  return (
    <SafeAreaView style={{ backgroundColor: Colors.white, flex: 1 }}>
      <ScreenGlow />
      <Box padding={Sizes[9]} flex={1}>
        <StatusBar style="dark" />
        <Logo />
        <Box flex={1} justifyContent="center">
          <TopProfile />
        </Box>
      </Box>
    </SafeAreaView>
  );
};
