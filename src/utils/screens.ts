import { Values } from "./values";

export const SCREENS = {
  LANDING: "Landing",
  SIGN_IN: "Signin",
  SIGN_UP: "Signup",
  ROOT: "Root",
  DISCOVER: "Discover",
  LIKES: "Likes",
  INBOX: "Inbox",
  ACCOUNT: "Account",
  PROFILE: "Profile",
  EDIT_PROFILE: "EditProfile",
} as const;

export type Screen = Values<typeof SCREENS>;
