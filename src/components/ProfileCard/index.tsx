import React from "react";
import { Image, StyleSheet, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

import { Box } from "../Box";
import { definitions } from "../../types/supabase";
import { Colors, Sizes } from "../../theme";
import { Heading } from "../Heading";
import { Text } from "../Text";

interface Props {
  profile: definitions["profiles"];
}

const styles = StyleSheet.create({
  fill: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export const ProfileCard = ({ profile }: Props): JSX.Element => {
  return (
    <Box overflow="hidden" backgroundColor={Colors.black}>
      <Image
        source={{ uri: profile.avatar_url }}
        style={{ aspectRatio: 0.75 }}
      />
      <LinearGradient
        colors={["transparent", "rgba(0,0,0,0.75)"]}
        style={styles.fill}
      />
      <View style={styles.fill}>
        <Box padding={Sizes[8]} flex={1} justifyContent="flex-end">
          <Heading fontSize={Sizes[6]} color={Colors.white}>
            {profile.name}
          </Heading>
          {profile.tagline && (
            <Text color={Colors.white}>{profile.tagline}</Text>
          )}
        </Box>
      </View>
    </Box>
  );
};
