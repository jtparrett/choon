import { useMutation } from "react-query";
import { useSession } from "../../contexts/session";
import { supabase } from "../supabase";

interface Props {
  onSuccess: () => void;
}

export const useDislikeProfile = ({ onSuccess }: Props) => {
  const session = useSession();
  return useMutation<unknown, unknown, string>(
    async (receiverProfileId) => {
      await supabase.from("dislikes").insert([
        {
          sender: session?.user?.id,
          receiver: receiverProfileId,
        },
      ]);
    },
    {
      onSuccess,
    }
  );
};
