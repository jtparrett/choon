import React, { ReactNode } from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  TouchableOpacityProps,
} from "react-native";
import { Box } from "../Box";
import { Colors } from "../../theme";

interface Props extends TouchableOpacityProps {
  icon: ReactNode;
  size?: number;
  variant?: "primary" | "secondary";
  isLoading?: boolean;
}

export const IconButton = ({
  icon,
  size = 80,
  variant = "primary",
  isLoading,
  style,
  disabled,
  ...props
}: Props): JSX.Element => {
  const bgc = {
    primary: Colors.black,
    secondary: Colors.gray[800],
  }[variant];

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[
        {
          width: size,
          height: size,
          backgroundColor: bgc,
          borderRadius: 999,
          opacity: disabled ? 0.8 : 1,
          alignItems: "center",
          justifyContent: "center",
        },
        style,
      ]}
      disabled={disabled}
      {...props}
    >
      <Box opacity={isLoading ? 0 : 1}>{icon}</Box>
      {isLoading && (
        <ActivityIndicator
          style={{ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }}
          color={Colors.white}
        />
      )}
    </TouchableOpacity>
  );
};
