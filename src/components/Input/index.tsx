import React from "react";
import { TextInput, TextInputProps, ViewStyle } from "react-native";
import { Colors, Radius, Sizes } from "../../theme";
import { extractViewStyleProps } from "../../utils/styles";

interface Props extends TextInputProps, ViewStyle {
  isInvalid?: boolean;
}

export const Input = ({ isInvalid, ...props }: Props): JSX.Element => {
  const style = extractViewStyleProps(props);

  return (
    <TextInput
      style={[
        {
          padding: Sizes[3],
          backgroundColor: Colors.gray[200],
          borderRadius: Radius.sm,
          borderWidth: 2,
          borderColor: isInvalid ? Colors.error : Colors.gray[200],
        },
        style,
      ]}
      {...props}
    />
  );
};
