import React from "react";
import { TextStyle } from "react-native";
import { Heading } from "../Heading";
import { Colors, Sizes } from "../../theme";

interface Props extends TextStyle {
  subText?: string;
}

export const Logo = ({ subText, ...style }: Props): JSX.Element => {
  return (
    <Heading fontSize={Sizes[7]} {...style}>
      CHOON
      {subText && <Heading color={Colors.gray[500]}> {subText}</Heading>}
    </Heading>
  );
};
