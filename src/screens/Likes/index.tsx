import { useNavigation } from "@react-navigation/core";
import React, { useEffect } from "react";
import {
  ActivityIndicator,
  Dimensions,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { useQuery } from "react-query";
import {
  Box,
  Heading,
  Logo,
  ProfileCard,
  Text,
  UserQueryError,
} from "../../components";
import { Colors, Fonts, Radius, Sizes } from "../../theme";
import { definitions } from "../../types/supabase";
import { SCREENS } from "../../utils/screens";
import { supabase } from "../../utils/supabase";

const screenWidth = Dimensions.get("window").width;

export const LikesScreen = (): JSX.Element => {
  const navigation = useNavigation();
  const likesQuery = useQuery("userLikes", async () => {
    const { data, error } = await supabase.rpc<definitions["profiles"]>(
      "get_likes_received_profiles"
    );

    if (error) {
      throw error;
    }

    return data;
  });

  useEffect(() => {
    const focusHandler = () => {
      likesQuery.refetch();
    };

    navigation.addListener("focus", focusHandler);
    return () => {
      navigation.removeListener("focus", focusHandler);
    };
  }, []);

  const Likes = (): JSX.Element => {
    if (likesQuery.isLoading || likesQuery.isIdle) {
      return (
        <Box paddingTop={Sizes[4]}>
          <ActivityIndicator size="large" color={Colors.black} />
        </Box>
      );
    }

    if (likesQuery.isError) {
      return <UserQueryError />;
    }

    if (likesQuery.data === null || likesQuery.data.length <= 0) {
      return (
        <Box paddingHorizontal={Sizes[9]} flex={1} justifyContent="center">
          <Heading fontSize={Sizes[4]}>You don't have any likes yet.</Heading>
        </Box>
      );
    }

    return (
      <ScrollView
        horizontal
        contentContainerStyle={{
          alignItems: "center",
          paddingHorizontal: Sizes[8],
        }}
      >
        {likesQuery.data.map((sender) => (
          <TouchableOpacity
            activeOpacity={0.8}
            key={sender.id}
            onPress={() => {
              // @ts-ignore
              navigation.navigate(SCREENS.PROFILE, {
                userId: sender.id,
              });
            }}
            style={{
              width: screenWidth - Sizes[9] * 2,
              marginHorizontal: Sizes[2],
              overflow: "hidden",
              borderRadius: Radius.lg,
            }}
          >
            <ProfileCard profile={sender} />
          </TouchableOpacity>
        ))}
      </ScrollView>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <Box paddingTop={Sizes[9]} paddingHorizontal={Sizes[9]}>
        <Logo subText="Likes" />
        <Text
          color={Colors.gray[500]}
          fontSize={Sizes[5]}
          fontFamily={Fonts.Bold}
        >
          Here's some people who like you.
        </Text>
      </Box>
      <Likes />
    </SafeAreaView>
  );
};
