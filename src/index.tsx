import React from "react";
import {
  useFonts,
  Inter_900Black,
  Inter_500Medium,
  Inter_700Bold,
} from "@expo-google-fonts/inter";
import AppLoading from "expo-app-loading";
import { NavigationContainer } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import { QueryClientProvider } from "react-query";
import "react-native-url-polyfill/auto";

import { AuthScreens } from "./screens/Auth";
import { Provider as SessionProvider, useSession } from "./contexts/session";
import { queryClient } from "./utils/queryClient";
import { RootScreens } from "./screens/Root";

import { OnboardingScreens } from "./screens/Onboarding";
import { useUserProfile } from "./utils/hooks/useUserProfile";

const AuthRoot = (): JSX.Element => {
  const [fontsLoaded] = useFonts({
    Inter_900Black,
    Inter_700Bold,
    Inter_500Medium,
  });

  const session = useSession();
  const profileQuery = useUserProfile();

  if (!fontsLoaded || profileQuery.isLoading) {
    return <AppLoading />;
  }

  if (!session) {
    return <AuthScreens />;
  }

  if (profileQuery.data === null) {
    return <OnboardingScreens />;
  }

  return <RootScreens />;
};

export const App = (): JSX.Element => {
  return (
    <QueryClientProvider client={queryClient}>
      <NavigationContainer>
        <SessionProvider>
          <AuthRoot />
        </SessionProvider>
      </NavigationContainer>
      <Toast />
    </QueryClientProvider>
  );
};
