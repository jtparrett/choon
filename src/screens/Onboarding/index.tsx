import React from "react";
import { KeyboardAvoidingView, SafeAreaView, ScrollView } from "react-native";
import { Box, Logo, Text } from "../../components";
import { ProfileForm } from "../../components/ProfileForm";
import { Colors, Fonts, Sizes } from "../../theme";

export const OnboardingScreens = (): JSX.Element => {
  return (
    <KeyboardAvoidingView behavior="height" style={{ flex: 1 }}>
      <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
        <ScrollView keyboardShouldPersistTaps="never">
          <Box padding={Sizes[7]}>
            <Logo subText="Welcome" />
            <Text
              color={Colors.gray[500]}
              fontSize={Sizes[5]}
              fontFamily={Fonts.Bold}
              marginBottom={Sizes[6]}
            >
              Tell us about yourself{"\n"}by filling-in your public profile.
            </Text>
            <ProfileForm buttonText="Next" />
          </Box>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};
