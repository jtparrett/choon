create or replace function get_likes_received_profiles() 
returns setof profiles
language sql
as $$
  select * from profiles 
    where id in (
      select sender from likes where receiver = auth.uid()
    ) and id not in (
      select receiver from dislikes where sender = auth.uid()
      union
      select matches.match from matches where matches.user = auth.uid()
    );
$$;
