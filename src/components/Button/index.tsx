import React, { ReactNode } from "react";
import {
  ActivityIndicator,
  TouchableOpacity,
  TouchableOpacityProps,
  ViewStyle,
} from "react-native";
import { Text } from "../Text";
import { Colors, Fonts, Radius, Sizes } from "../../theme";
import { extractViewStyleProps } from "../../utils/styles";

interface Props extends TouchableOpacityProps, ViewStyle {
  children: ReactNode;
  variant?: "primary" | "secondary";
  isLoading?: boolean;
}

export const Button = ({
  children,
  variant = "primary",
  isLoading,
  disabled,
  ...props
}: Props): JSX.Element => {
  const style = extractViewStyleProps(props);

  const bgc = {
    primary: Colors.black,
    secondary: Colors.gray[300],
  }[variant];

  const textc = {
    primary: Colors.white,
    secondary: Colors.black,
  }[variant];

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[
        {
          backgroundColor: bgc,
          padding: Sizes[4],
          borderRadius: Radius.md,
          opacity: disabled ? 0.8 : 1,
        },
        style,
      ]}
      disabled={disabled}
      {...props}
    >
      <Text
        color={textc}
        textAlign="center"
        fontFamily={Fonts.Bold}
        opacity={isLoading ? 0 : 1}
      >
        {children}
      </Text>
      {isLoading && (
        <ActivityIndicator
          style={{ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }}
          color={textc}
        />
      )}
    </TouchableOpacity>
  );
};
