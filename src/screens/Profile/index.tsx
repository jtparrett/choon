import { RouteProp, useNavigation, useRoute } from "@react-navigation/core";
import React from "react";
import { ActivityIndicator, SafeAreaView, ScrollView } from "react-native";
import { CheckIcon, XIcon } from "react-native-heroicons/solid";
import { useQuery, useQueryClient } from "react-query";

import {
  BackArrow,
  Box,
  IconButton,
  ProfileCard,
  Text,
  UserQueryError,
} from "../../components";
import { Colors, Sizes } from "../../theme";
import { definitions } from "../../types/supabase";
import { useDislikeProfile } from "../../utils/hooks/useDislikeProfile";
import { useLikeProfile } from "../../utils/hooks/useLikeProfile";
import { supabase } from "../../utils/supabase";
import { RootStackParamList } from "../Root";

type ProfileScreenRouteProp = RouteProp<RootStackParamList, "Profile">;

export const ProfileScreen = (): JSX.Element => {
  const route = useRoute<ProfileScreenRouteProp>();
  const profileQuery = useQuery(["profile", route.params.userId], async () => {
    const { data } = await supabase
      .from<definitions["profiles"]>("profiles")
      .select("*")
      .eq("id", route.params.userId)
      .single();

    return data;
  });

  const Profile = (): JSX.Element => {
    const navigation = useNavigation();
    const queryClient = useQueryClient();
    const likeProfileMutation = useLikeProfile({
      async onSuccess() {
        await queryClient.refetchQueries(["discover"]);
        navigation.goBack();
      },
    });

    const dislikeProfileMutation = useDislikeProfile({
      async onSuccess() {
        await queryClient.refetchQueries(["discover"]);
        navigation.goBack();
      },
    });

    if (profileQuery.isLoading || profileQuery.isIdle) {
      return (
        <Box flex={1} alignItems="center" justifyContent="center">
          <ActivityIndicator size="large" color={Colors.white} />
        </Box>
      );
    }

    if (profileQuery.isError || profileQuery.data === null) {
      return (
        <SafeAreaView>
          <UserQueryError />
        </SafeAreaView>
      );
    }

    const profile = profileQuery.data;

    return (
      <>
        <ScrollView>
          <ProfileCard profile={profile} />
          <Box padding={Sizes[8]}>
            <Text color={Colors.white}>{profile.bio}</Text>
          </Box>
        </ScrollView>

        <SafeAreaView
          style={{
            position: "absolute",
            bottom: 0,
            left: 0,
            width: "100%",
            zIndex: 2,
          }}
        >
          <Box
            flexDirection="row"
            justifyContent="space-between"
            paddingHorizontal={Sizes[7]}
            paddingBottom={Sizes[4]}
          >
            <IconButton
              variant="secondary"
              icon={<XIcon size={Sizes[8]} color={Colors.white} />}
              isLoading={dislikeProfileMutation.isLoading}
              disabled={dislikeProfileMutation.isLoading}
              onPress={() => dislikeProfileMutation.mutate(profile.id)}
            />
            <IconButton
              variant="secondary"
              icon={<CheckIcon size={Sizes[8]} color={Colors.white} />}
              isLoading={likeProfileMutation.isLoading}
              disabled={likeProfileMutation.isLoading}
              onPress={() => likeProfileMutation.mutate(profile.id)}
            />
          </Box>
        </SafeAreaView>
      </>
    );
  };

  return (
    <Box flex={1} backgroundColor={Colors.black}>
      <SafeAreaView
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          zIndex: 2,
        }}
      >
        <Box paddingHorizontal={Sizes[7]} paddingTop={Sizes[5]}>
          <BackArrow
            height={Sizes[9]}
            width={Sizes[9]}
            backgroundColor={Colors.white}
            borderRadius={999}
            alignItems="center"
            justifyContent="center"
          />
        </Box>
      </SafeAreaView>

      <Profile />
    </Box>
  );
};
