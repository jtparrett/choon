import { useMutation } from "react-query";
import { useSession } from "../../contexts/session";
import { definitions } from "../../types/supabase";
import { supabase } from "../supabase";

interface Props {
  onSuccess: () => void;
}

export const useLikeProfile = ({ onSuccess }: Props) => {
  const session = useSession();
  return useMutation<unknown, unknown, string>(
    async (receiverProfileId) => {
      const { data: existingLike } = await supabase
        .from<definitions["likes"]>("likes")
        .select("id")
        .eq("sender", receiverProfileId)
        .eq("receiver", session?.user?.id)
        .maybeSingle();

      if (existingLike) {
        await supabase.from("matches").insert([
          {
            user: session?.user?.id,
            match: receiverProfileId,
          },
          {
            user: receiverProfileId,
            match: session?.user?.id,
          },
        ]);

        return;
      }

      await supabase.from("likes").insert([
        {
          sender: session?.user?.id,
          receiver: receiverProfileId,
        },
      ]);
    },
    {
      onSuccess,
    }
  );
};
