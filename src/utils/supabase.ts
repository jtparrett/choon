import { createClient } from "@supabase/supabase-js";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Constants from "expo-constants";

if (!Constants.manifest?.extra?.supabaseUrl) {
  throw new Error("Missing PUBLIC_SUPABASE_URL env variable");
}

if (!Constants.manifest?.extra?.supabaseAnon) {
  throw new Error("Missing PUBLIC_SUPABASE_ANON_KEY env variable");
}

export const supabase = createClient(
  Constants.manifest.extra.supabaseUrl,
  Constants.manifest.extra.supabaseAnon,
  {
    localStorage: AsyncStorage,
  }
);
