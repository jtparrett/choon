import { useFormik } from "formik";
import React from "react";
import { SafeAreaView } from "react-native";
import { useMutation } from "react-query";
import * as Yup from "yup";
import Toast from "react-native-toast-message";

import {
  BackArrow,
  Box,
  Button,
  Input,
  Logo,
  FormField,
} from "../../components";
import { Colors, Sizes } from "../../theme";
import { errorWithMessage } from "../../utils/errorWithMessage";
import { supabase } from "../../utils/supabase";

const validationSchema = Yup.object({
  email: Yup.string().email().required(),
  password: Yup.string().required(),
});

type FormValues = Yup.InferType<typeof validationSchema>;

export const SignupScreen = (): JSX.Element => {
  const signupMutation = useMutation<unknown, unknown, FormValues>(
    async ({ email, password }) => {
      const { error } = await supabase.auth.signUp({
        email: email.toLowerCase(),
        password,
      });

      if (error) {
        throw error;
      }
    },
    {
      onError(error) {
        const validError = errorWithMessage(error);

        if (validError) {
          Toast.show({
            type: "error",
            text1: validError.message,
          });
        }
      },
    }
  );

  const formik = useFormik({
    validationSchema,
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit({ email, password }) {
      signupMutation.mutate({ email, password });
    },
  });

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <Box flex={1} paddingHorizontal={Sizes[9]} paddingTop={Sizes[4]}>
        <BackArrow />
        <Logo subText="Signup" marginTop={Sizes[3]} />

        <FormField
          label="Email Address"
          error={formik.errors.email}
          marginTop={Sizes[6]}
        >
          <Input
            isInvalid={!!formik.errors.email}
            onChangeText={(value) => formik.setFieldValue("email", value)}
          />
        </FormField>

        <FormField
          label="Password"
          error={formik.errors.password}
          marginTop={Sizes[3]}
        >
          <Input
            secureTextEntry
            isInvalid={!!formik.errors.password}
            onChangeText={(value) => formik.setFieldValue("password", value)}
          />
        </FormField>

        <Box marginTop={Sizes[6]}>
          <Button
            onPress={() => formik.handleSubmit()}
            isLoading={signupMutation.isLoading}
            disabled={
              signupMutation.isLoading || !(formik.isValid && formik.dirty)
            }
          >
            Create account
          </Button>
        </Box>
      </Box>
    </SafeAreaView>
  );
};
