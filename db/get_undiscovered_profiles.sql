create or replace function get_undiscovered_profiles() 
returns setof profiles
language sql
as $$
  select * from profiles 
    where id not in (
      select receiver as id from likes where sender = auth.uid()
      union
      select sender as id from dislikes where receiver = auth.uid()
      union
      select receiver as id from dislikes where sender = auth.uid()
      union
      select matches.match as id from matches where matches.user = auth.uid()
    );
$$;
