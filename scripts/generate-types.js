const openapiTS = require("openapi-typescript").default;
const fs = require("fs");
require("dotenv").config({
  path: ".env",
});

const { PUBLIC_SUPABASE_URL, PUBLIC_SUPABASE_ANON_KEY } = process.env;

if (!PUBLIC_SUPABASE_URL) {
  throw new Error("missing PUBLIC_SUPABASE_URL from env");
}

if (!PUBLIC_SUPABASE_ANON_KEY) {
  throw new Error("missing PUBLIC_SUPABASE_ANON_KEY from env");
}

openapiTS(
  `${PUBLIC_SUPABASE_URL}/rest/v1/?apikey=${PUBLIC_SUPABASE_ANON_KEY}`
).then((output) => {
  fs.writeFile("./src/types/supabase.ts", output, (err) => new Error(err));
});
