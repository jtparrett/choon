export const Colors = {
  black: "#131313",
  white: "#ffffff",
  pro: "#9442FF",
  error: "#dd0000",
  gray: {
    200: "#f5f5f5",
    300: "#f2f2f2",
    500: "#C9C9C9",
    800: "#292929",
  },
};

export const Fonts = {
  Black: "Inter_900Black",
  Bold: "Inter_700Bold",
  Medium: "Inter_500Medium",
};

export const Sizes = {
  1: 4,
  2: 8,
  3: 12,
  4: 16,
  5: 18,
  6: 22,
  7: 28,
  8: 32,
  9: 42,
  10: 64,
};

export const Radius = {
  sm: 8,
  md: 12,
  lg: 28,
};
