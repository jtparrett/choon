import { useQuery } from "react-query";
import { useSession } from "../../contexts/session";
import { definitions } from "../../types/supabase";
import { supabase } from "../supabase";

export const useUserProfile = () => {
  const session = useSession();

  return useQuery(
    "userProfile",
    async () => {
      const { data, error } = await supabase
        .from<definitions["profiles"]>("profiles")
        .select("*")
        .eq("id", session?.user?.id)
        .maybeSingle();

      if (error) {
        throw error;
      }

      return data;
    },
    {
      enabled: !!session,
    }
  );
};
