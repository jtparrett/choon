import { useFormik } from "formik";
import React from "react";
import { useMutation } from "react-query";
import * as Yup from "yup";

import { Button, FormField, Input } from "..";
import { useSession } from "../../contexts/session";
import { Sizes } from "../../theme";
import { useUserProfile } from "../../utils/hooks/useUserProfile";
import { supabase } from "../../utils/supabase";

interface Props {
  buttonText?: string;
  onSuccess?: () => void;
}

const validationSchema = Yup.object({
  name: Yup.string().required(),
  tagline: Yup.string().required(),
  bio: Yup.string().required(),
});

type FormValues = Yup.InferType<typeof validationSchema>;

export const ProfileForm = ({
  buttonText = "Save Changes",
  onSuccess,
}: Props): JSX.Element => {
  const session = useSession();
  const profileQuery = useUserProfile();
  const profileMutation = useMutation<unknown, unknown, FormValues>(
    async ({ name, tagline, bio }) => {
      await supabase.from("profiles").upsert({
        id: session?.user?.id,
        name,
        tagline,
        bio,
      });
    },
    {
      async onSuccess() {
        await profileQuery.refetch();
        onSuccess?.();
      },
    }
  );

  const existingProfile = profileQuery.data;
  const formik = useFormik({
    validationSchema,
    enableReinitialize: true,
    initialValues: {
      name: existingProfile?.name ?? "",
      tagline: existingProfile?.tagline ?? "",
      bio: existingProfile?.bio ?? "",
    },
    onSubmit(values) {
      profileMutation.mutate(values);
    },
  });

  return (
    <>
      <FormField
        label="Name"
        error={formik.errors.name}
        marginBottom={Sizes[3]}
      >
        <Input
          isInvalid={!!formik.errors.name}
          value={formik.values.name}
          onChangeText={(value) => formik.setFieldValue("name", value)}
        />
      </FormField>

      <FormField
        label="Tagline"
        error={formik.errors.tagline}
        marginBottom={Sizes[3]}
      >
        <Input
          isInvalid={!!formik.errors.tagline}
          value={formik.values.tagline}
          onChangeText={(value) => formik.setFieldValue("tagline", value)}
        />
      </FormField>

      <FormField
        label="Profile Bio"
        error={formik.errors.bio}
        marginBottom={Sizes[8]}
      >
        <Input
          multiline
          minHeight={100}
          isInvalid={!!formik.errors.bio}
          value={formik.values.bio}
          onChangeText={(value) => formik.setFieldValue("bio", value)}
        />
      </FormField>

      <Button
        isLoading={profileMutation.isLoading}
        disabled={
          profileMutation.isLoading || !(formik.isValid && formik.dirty)
        }
        onPress={() => formik.handleSubmit()}
      >
        {buttonText}
      </Button>
    </>
  );
};
