import React from "react";
import { Colors, Sizes } from "../../theme";
import { Box, BoxProps } from "../Box";
import { Text } from "../Text";

interface Props extends BoxProps {
  label: string;
  error?: string;
}

export const FormField = ({
  children,
  label,
  error,
  ...boxProps
}: Props): JSX.Element => {
  return (
    <Box {...boxProps}>
      <Text marginBottom={Sizes[2]}>{label}</Text>
      {children}
      {error && (
        <Text marginTop={Sizes[1]} color={Colors.error}>
          {error}
        </Text>
      )}
    </Box>
  );
};
