import { useNavigation } from "@react-navigation/core";
import React from "react";
import { KeyboardAvoidingView, SafeAreaView, ScrollView } from "react-native";
import { BackArrow, Box, Logo } from "../../components";
import { ProfileForm } from "../../components/ProfileForm";
import { Colors, Sizes } from "../../theme";

export const EditProfileScreen = (): JSX.Element => {
  const navigation = useNavigation();
  return (
    <KeyboardAvoidingView behavior="height" style={{ flex: 1 }}>
      <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
        <ScrollView keyboardShouldPersistTaps="never">
          <Box
            paddingHorizontal={Sizes[9]}
            paddingTop={Sizes[4]}
            paddingBottom={Sizes[9]}
          >
            <BackArrow />
            <Logo
              subText="Edit Profile"
              marginTop={Sizes[3]}
              marginBottom={Sizes[6]}
            />
            <ProfileForm onSuccess={() => navigation.goBack()} />
          </Box>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};
