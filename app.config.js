require("dotenv").config({
  path: ".env",
});

export default {
  name: "choon",
  slug: "choon",
  version: "1.0.0",
  orientation: "portrait",
  icon: "./assets/icon.png",
  splash: {
    image: "./assets/splash.png",
    resizeMode: "contain",
    backgroundColor: "#000000",
  },
  updates: {
    fallbackToCacheTimeout: 0,
  },
  assetBundlePatterns: ["**/*"],
  ios: {
    supportsTablet: false,
  },
  android: {
    adaptiveIcon: {
      foregroundImage: "./assets/adaptive-icon.png",
      backgroundColor: "#FFFFFF",
    },
  },
  extra: {
    supabaseUrl: process.env.PUBLIC_SUPABASE_URL,
    supabaseAnon: process.env.PUBLIC_SUPABASE_ANON_KEY,
  },
};
