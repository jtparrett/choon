import React from "react";
import {
  ActivityIndicator,
  Image,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { useQuery } from "react-query";
import { Box, Heading, Logo, Text, UserQueryError } from "../../components";
import { useSession } from "../../contexts/session";
import { Colors, Fonts, Sizes } from "../../theme";
import { definitions } from "../../types/supabase";
import { supabase } from "../../utils/supabase";

interface MatchesWithMatchProfile
  extends Omit<definitions["matches"], "match"> {
  match: definitions["profiles"];
}

export const InboxScreen = (): JSX.Element => {
  const session = useSession();
  const matchesQuery = useQuery("userMatches", async () => {
    const { data, error } = await supabase
      .from<MatchesWithMatchProfile>("matches")
      .select("user, match (*)")
      .eq("user", session?.user?.id);

    if (error) {
      throw error;
    }

    return data;
  });

  const Matches = (): JSX.Element => {
    if (matchesQuery.isLoading || matchesQuery.isIdle) {
      return (
        <Box paddingTop={Sizes[6]}>
          <ActivityIndicator size="large" color={Colors.black} />
        </Box>
      );
    }

    if (matchesQuery.isError) {
      return <UserQueryError />;
    }

    if (matchesQuery.data === null || matchesQuery.data.length <= 0) {
      return (
        <Box flex={1} justifyContent="center">
          <Heading fontSize={Sizes[4]}>You don't have any matches yet.</Heading>
        </Box>
      );
    }

    return (
      <ScrollView contentContainerStyle={{ paddingTop: Sizes[4] }}>
        {matchesQuery.data.map(({ match }) => (
          <Box
            key={match.id}
            flexDirection="row"
            alignItems="center"
            marginBottom={Sizes[4]}
          >
            <Image
              source={{ uri: match.avatar_url }}
              style={{
                width: 60,
                height: 60,
                borderRadius: 999,
              }}
            />
            <Box paddingLeft={Sizes[3]} flex={1}>
              <Text fontSize={Sizes[4]} fontFamily={Fonts.Bold}>
                {match.name}
              </Text>
              <Text fontSize={Sizes[3]}>
                Start the conversation with your new match...
              </Text>
            </Box>
          </Box>
        ))}
      </ScrollView>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.white }}>
      <Box padding={Sizes[9]} flex={1}>
        <Logo subText="Inbox" />
        <Text
          color={Colors.gray[500]}
          fontSize={Sizes[5]}
          fontFamily={Fonts.Bold}
        >
          Here you can message your matches.
        </Text>
        <Matches />
      </Box>
    </SafeAreaView>
  );
};
