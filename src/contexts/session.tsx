import React, {
  useEffect,
  createContext,
  useState,
  ReactNode,
  useContext,
} from "react";
import { AuthSession } from "@supabase/supabase-js";
import { supabase } from "../utils/supabase";
import { useQueryClient } from "react-query";

const Context = createContext<AuthSession | null>(null);

interface Props {
  children: ReactNode;
}

export const Provider = ({ children }: Props): JSX.Element => {
  const [session, setSession] = useState<AuthSession | null>(null);
  const queryClient = useQueryClient();

  useEffect(() => {
    setSession(supabase.auth.session());

    const listener = supabase.auth.onAuthStateChange((event, session) => {
      if (event === "SIGNED_OUT") {
        queryClient.clear();
      }

      setSession(session);
    });

    return () => {
      listener.data?.unsubscribe();
    };
  }, []);

  return <Context.Provider value={session}>{children}</Context.Provider>;
};

export const useSession = () => useContext(Context);
